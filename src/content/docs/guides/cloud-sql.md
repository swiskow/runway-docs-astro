---
title: Connect a service to Cloud SQL
description: How to connect your Runway service to a Cloud SQL database.
---

Automatic configuration of a Cloud SQL connection to servces
[is still in the works](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/54).
In the meantime, service maintainers can configure a connection themselves. The following
procedure uses [IAM database authentication](https://cloud.google.com/sql/docs/postgres/iam-logins)
for passwordless logins:

- Locate the Cloud Run service account name:
```sh
gcloud run services describe $SERVICE_NAME --project=$GCP_PROJECT --region=$GCP_REGION --format=json | jq -r .spec.template.spec.serviceAccountName
```
- Assign to the service account the IAM roles `Cloud SQL Client` and `Cloud SQL Instance User`
  in the project in which the Cloud SQL instance resides
- Enable IAM authentication in your Cloud SQL instance:
```
gcloud sql instances patch $CLOUD_SQL_INSTANCE --database-flags=cloudsql.iam_authentication=on --project=GCP_PROJECT
```
-  Create a database user in your Cloud SQL instance associated with the service account name. Note that due to Postgres
   usernames length limit, the user name does not include the `.gserviceaccount.com` suffix:
```sh
gcloud sql users create $SA_SHORT_NAME --instance=$CLOUD_SQL_INSTANCE --project $GCP_PROJECT --type=cloud_iam_service_account
```
- Grant permissions to the user in Postgres:
```sql
GRANT cloudsqlsuperuser to $SA_SHORT_NAME;
```
- Substitute your service's Dockerfile `CMD` command with a startup script that
  starts the [Cloud SQL Auth Proxy](https://cloud.google.com/sql/docs/postgres/sql-proxy) in the background,
  specifying your Cloud SQL instance connection name, and with the `--auto-iam-authn` option (see an example
  script [here](https://gitlab.com/gitlab-org/modelops/code-suggestions-model-evaluation/model-evaluator/-/blob/ee11ce023c9bec23304fd7f2804dea267791b728/eval-code-viewer/scripts/start.sh))
- Configure your application to connect to host `127.0.0.1`, port `5432`, and use
  the database username created in the previous steps. You can use `.runway/env-staging.yml`
  to configure these values (see an example MR [here](https://gitlab.com/gitlab-org/modelops/code-suggestions-model-evaluation/model-evaluator/-/merge_requests/27))

For an example of the entire process, see [this issue](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2421).

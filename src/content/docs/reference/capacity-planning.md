---
title: Capacity Planning
description: This describes how to do capacity planning on Runway
---

Runway supports [capacity planning](https://about.gitlab.com/handbook/engineering/infrastructure/capacity-planning/) for saturation resources of a service. To view forecasts, refer to [Tamland page](https://gitlab-com.gitlab.io/gl-infra/tamland/runway.html).

## Setup

Right now, prerequisite for capacity planning is service catalog entry. For setup, refer to [documentation](../observability#setup). Forecasting requires 2 weeks worth of data after the provisioning of a service.

## Container Memory Utilization

The container memory utilization of a service distributed across all container instances.

For scaling, refer to [documentation](../scalability#Memory).

## Container CPU Utilization

The container CPU utilization of a service distributed across all container instances.

For scaling, refer to [documentation](../scalability#CPU).

## Container Instance Utilization

The container instance utilization of a service.

For scaling, refer to [documentation](../scalability#Maximum-instances).

## Container Max Concurrent Requests

The maxmimum number of concurrent requests being served by each container instance of a service.

For scaling, refer to [documentation](../scalability#Maxmimum-instance-concurrent-requests).